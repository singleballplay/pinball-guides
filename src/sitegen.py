import collections
import json
import logging
import markdown
import os
import shutil
import sys
from jinja2 import (
    Environment,
    FileSystemLoader,
    Template,
)


logging.basicConfig(level=logging.INFO)


def main():
    output_dir = sys.argv[1] if len(sys.argv) > 1 else 'public'
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    mypath = 'assets'
    for f in os.listdir(mypath):
        fullname = os.path.join(mypath, f)
        if os.path.isfile(fullname):
            shutil.copy(fullname, output_dir)

    with open('db/machines.json') as data_file:
        data = json.load(data_file)

    for machine, machine_data in data.items():
        filename = 'machines/{0}.md'.format(machine)
        if os.path.isfile(filename):
            with open(filename) as md_content:
                machine_data['content'] = markdown.markdown(md_content.read())

    logging.info('ordering machine list')
    # order the json data
    machines = collections.OrderedDict(sorted(data.items(), key=lambda x : x[0]))

    env = Environment(loader=FileSystemLoader('templates'))
    logging.info('loading jinja template')
    template = env.get_template('index.html.j2')

    with open('{0}/{1}'.format(output_dir, 'index.html'), 'w') as output_file:
        logging.info('writing file')
        output_file.write(template.render(machines=list(machines.items())))


main()
