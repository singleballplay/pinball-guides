plunge "King" lanes. "kings" increase red multiplier for when completing drop targets. multiplier stays from ball to ball. king lanes only advance one way with right flipper.

drop targets need to be completed in order, otherwise it reduces the value of the drops. try and shoot targets and kings from left flipper Q maybe from right flipper.

bonus multipliers are increased by hitting blue joker targets.

can ignore spinner and go for kings and drops and 3x bonus advance from top hole.

bonus can be significant with multipliers.
