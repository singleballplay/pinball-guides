<a href="https://pintips.net/games/241" class="pintips">view pintips</a>

Rollover A,B,C,D to light drop targets for higher value. Dropping all the standups once will light an alternating standup on the right for double advance and the other to reset drops. Dropping and resetting can be worth good points. Otherwise shoot for saucer and try to rollover lit rollovers.
