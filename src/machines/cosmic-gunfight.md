Shoot right orbit to advance values and light 2x playfield score for the rest of the ball. Also to advance bonus X. Probably the easiest points strategy, keep hitting the right orbit.

Complete top 1-2-3 lanes to light multiball locks (upper left and left orbit). Can repeat as often as necessary.

Complete 9 insert grid under left standups to enable end of game bonus time (cosmic time), which increases based on various playfield shots. Can change lit row holding one flipper and tapping the other.

Completing X-Y lanes advances center eject value.
