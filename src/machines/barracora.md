Completing each target bank lights a lock and adds a multiplier (yellow per/ball). Completing both will light a green multiplier that stays through the game.
The two multipliers are multiplied together e.g. 2x (g) 3x (y) = 6x bonus. ACORA bank lights upper lock, BARR bank lights lower saucer.

To light botton lanes (4,5,6) the light above must be lit (1,2,3). Each completion of 4,5,6 lights values for upper right target and stay throughout game.
Finishing 1,2,3 & 4,5,6 lights upper saucer for bonus collect.

Ball locked in upper saucer will release if the second ball is drained.

Spinner advances bonus greatly.
